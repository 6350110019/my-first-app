import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirsApp());
}

class MyFirsApp extends StatelessWidget {
  const MyFirsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon_rounded),
          title: Text('My First App'),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.add_alarm)),
            IconButton(onPressed: () {}, icon: Icon(Icons.account_circle)),
          ],
        ),
        body: Center(
            child: Column(children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border.all(
                color: Colors.lightGreen,
                width: 10,
              ),
            ),
            child: Image.asset('assets/me.jpg',
                width: 400, height: 300, fit: BoxFit.cover),
          ),
          Text('นาย ศุภณัฐ อักษรสิทธิ์ 6350110019 ',
              style: TextStyle(
                  height: 2, fontSize: 20, fontWeight: FontWeight.bold)),
        ])),
      ),
    );
  }
}
